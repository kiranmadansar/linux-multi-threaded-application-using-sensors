#ifndef i2c_h
#define i2c_h


#include<linux/i2c-dev.h>
#include<sys/ioctl.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<stdint.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<unistd.h>
#include<string.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <sys/syscall.h>

extern sem_t *sem;

/* I2C Functions */

temp_s i2c_bus_open(int *file);

temp_s i2c_bus_init(int file, int sensor);

temp_s i2c_read_byte(int fd , uint8_t *byte);

temp_s i2c_read_word(int fd , uint8_t *byte);

temp_s i2c_write_byte(int fd , uint8_t byte);

temp_s i2c_write_word(int fd , uint8_t *byte);
temp_s i2c_write_word_light(int fd ,uint8_t * byte );


#endif
