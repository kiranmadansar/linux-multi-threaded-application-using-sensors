#include <stdio.h>
#include <semaphore.h>
#include <fcntl.h>
#include "usrled.h"
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "log.h"
#include <mqueue.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include "temp.h"
#include "light.h"
#include "socket.h"
#include <sys/types.h>
#include <fcntl.h>
#include "project.h"
#include "usrled.h"

mqd_t hbtemp_q, hblight_q, hbsock_q, hblog_q;
pid_t main_pid, sock_pid, log_pid, temp_pid, light_pid;
pthread_t heartbeat_thread;
volatile sig_atomic_t kill_process=0;
sem_t *my_sem;

void cleanup_process()
{
	if(sock_pid)
	{
		kill(sock_pid, SIGINT);
		waitpid(sock_pid, NULL, NULL);
		sock_pid = 0;
	}
	if(temp_pid)
	{
		kill(temp_pid, SIGINT);
		waitpid(temp_pid, NULL, NULL);
		temp_pid =0;
	}
	if(light_pid)
	{
		kill(light_pid, SIGINT);
		waitpid(light_pid, NULL, NULL);
		light_pid=0;	
	}
	usleep(5000);
	if(log_pid)
	{
		kill(log_pid, SIGINT);
		waitpid(log_pid, NULL, NULL);
		log_pid=0;
	}
}


pid_t create_process(uint8_t name, char *argv[])
{
	pid_t pid;
	pid = fork();
	if(pid < 0)
	{
		printf("Log Process Creation Failed\n");
		return -1;	
	}
	
	else if(pid == 0)
	{
		if(name == LOG_PROCESS)
		{
			char *args[]={"./log.out", argv[1], "&", NULL};
			execv(args[0], args);
		}
		else if(name==TEMP_PROCESS)
		{
			char *args[]={"./temp.out", "&", NULL};
			execv(args[0], args);
		}
		else if(name==LIGHT_PROCESS)
		{
			char *args[]={"./light.out", "&", NULL};
			execv(args[0], args);
		}
		else if(name==SOCK_PROCESS)
		{
			char *args[]={"./sock.out", "&", NULL};
			execv(args[0], args);
		}
	}
	
	else if(pid>0)
	{
		return pid;
	}
}

uint8_t create_queue()
{

	struct mq_attr attr;
	attr.mq_maxmsg = 5;
	attr.mq_msgsize = sizeof(uint8_t);

	struct mq_attr attr_log;
	attr_log.mq_maxmsg = 5;
	attr_log.mq_msgsize = sizeof(Logger_t);
	mq_unlink(LOG_QUEUE);
	if((logger_q = mq_open(LOG_QUEUE, O_RDWR | O_CREAT, 0666, &attr_log))==-1)
	{
		perror("LOGGER QUEUE");
		return -1;
	}

	if((hbtemp_q = mq_open(HBTEMP_QUEUE, O_RDWR | O_CREAT, 0666, &attr))==-1)
	{
		perror("HB_TEMP QUEUE");
		return -1;
	}

	if((hblight_q = mq_open(HBLIGHT_QUEUE, O_RDWR | O_CREAT, 0666, &attr))==-1)
	{
		perror("HB_LIGHT QUEUE");
		return -1;
	}
	
	if((hblog_q = mq_open(HBLOG_QUEUE, O_RDWR | O_CREAT, 0666, &attr))==-1)
	{
		perror("HB_LOG QUEUE");
		return -1;
	}

	if((hbsock_q = mq_open(HBSOCK_QUEUE, O_RDWR | O_CREAT, 0666, &attr))==-1)
	{
		perror("HB_SOCK QUEUE");
		return -1;
	}
	return 0;
}

void cleanup_queue()
{
	mq_close(logger_q);
	mq_unlink(LOG_QUEUE);
	mq_close(hbtemp_q);
	mq_unlink(HBTEMP_QUEUE);
	mq_close(hblight_q);
	mq_unlink(HBLIGHT_QUEUE);
	mq_close(hblog_q);
	mq_unlink(HBLOG_QUEUE);
	mq_close(hbsock_q);
	mq_unlink(HBSOCK_QUEUE);
}

void cleanup()
{
	kill_process = 1;
	cleanup_process();
	cleanup_queue();
	mq_close(logger_q);
	mq_unlink(LOG_QUEUE);
	sem_close(my_sem);
	sem_destroy(my_sem);
	exit(1);
}

int main(int argc, char *argv[])
{
	uint8_t result, val_send, val_rec, val;
	struct timespec tm;

	main_pid=0; 
	sock_pid=0; 
	log_pid=0; 
	temp_pid=0;
	light_pid=0;

	if(!argv[1])
	{
		printf("No Filename entered \n");
		return -1;
	}	

	if((create_queue())==-1)	return -1;
	
	
	temp_pid = create_process(TEMP_PROCESS, argv);
	if(temp_pid < 0)
	{
		cleanup_queue();
		cleanup_process();
		identification_led();
		return -1;
	}
	usleep(500);
	light_pid = create_process(LIGHT_PROCESS, argv);
	if(light_pid < 0)
	{
		cleanup_queue();
		cleanup_process();
		return -1;
	}

	
	usleep(500);
	log_pid = create_process(LOG_PROCESS, argv);
	if(log_pid < 0)
	{
		cleanup_queue();
		return -1;
	}
	
	usleep(500);
	sock_pid = create_process(SOCK_PROCESS, argv);
	if(sock_pid < 0)
	{
		cleanup_queue();
		cleanup_process();
		return -1;
	}

	if((my_sem = sem_open(SEM_NAME, O_CREAT | O_RDWR, 0666, 0))==SEM_FAILED)
	{
		perror("SEM OPEN:");
		cleanup_queue();
		cleanup_process();
		return -1;
	}
	sem_post(my_sem);
	sem_post(my_sem);
	sem_post(my_sem);
	sem_post(my_sem);
	LOG(INIT, MAIN_TASK, "MAIN TASK INITIALISED", NULL);
	uint8_t hblog_count=0, hbsock_count=0, hbtemp_count=0, hblight_count=0;
	signal(SIGINT, cleanup);
	//sleep(1);	
	while(!kill_process)
	{
		clock_gettime(CLOCK_MONOTONIC, &tm);
		tm.tv_sec += 3;
		tm.tv_nsec =0;
		if((mq_timedsend(hbtemp_q, &val_send, sizeof(val_send),0, &tm))==-1)
        	{	
			if((hbtemp_count++) > 10)
				LOG(ERROR, MAIN_TASK, "No heartbeat from TEMP", NULL);
        	}
		clock_gettime(CLOCK_MONOTONIC, &tm);
		tm.tv_sec += 3;
		tm.tv_nsec = 0; 
		if((mq_timedreceive(hbtemp_q, &val_rec, sizeof(val_rec), 0, &tm))==-1)
		{
			if((hbtemp_count++) > 10)
				LOG(ERROR, MAIN_TASK, "No heartbeat from TEMP", NULL);
		}
		else if(val_rec == (val_send+1))
		{
			LOG(HEARTBEAT, TEMP_TASK, "ALIVE: HEARTBEAT FROM TEMP", NULL);
		}
		val_send=0, val_rec=0;	
		clock_gettime(CLOCK_MONOTONIC, &tm);
		tm.tv_sec += 3; 
		tm.tv_nsec = 0;
		if((mq_timedsend(hblight_q, &val_send, sizeof(val_send),0, &tm))==-1)
        	{
			if((hblight_count++) > 10)
				LOG(ERROR, MAIN_TASK, "No heartbeat for LIGHT LOL", NULL);
        	}

		clock_gettime(CLOCK_MONOTONIC, &tm);
		tm.tv_sec += 3;
		tm.tv_nsec = 0; 
		if((mq_timedreceive(hblight_q, &val_rec, sizeof(val_rec), 0, &tm))==-1)
		{
			if((hblight_count++) > 10)
				LOG(ERROR, MAIN_TASK, "No heartbeat for LIGHT", NULL);
		}
		else if(val_rec == (val_send+1))
		{
			LOG(HEARTBEAT, LIGHT_TASK, "ALIVE: HEARTBEAT FROM LIGHT", NULL);
		}

		val_send=0, val_rec=0;		
		clock_gettime(CLOCK_MONOTONIC, &tm);
		tm.tv_sec += 3;
		tm.tv_nsec =0;
		if((mq_timedsend(hbsock_q, &val_send, sizeof(val_send),0, &tm))==-1)
        	{
			if((hbsock_count++) > 10)
				LOG(ERROR, MAIN_TASK, "No heartbeat for SOCK", NULL);
        	}

		clock_gettime(CLOCK_MONOTONIC, &tm);
		tm.tv_sec += 3;
		tm.tv_nsec =0;
		if((mq_receive(hbsock_q, &val_rec, sizeof(val_rec), 0))==-1)
		{
			if((hbsock_count++) > 10)
				LOG(ERROR, MAIN_TASK, "No heartbeat for SOCK", NULL);
		}
		else if(val_rec == (val_send+1))
		{
			LOG(HEARTBEAT, SOCK_TASK, "ALIVE: HEARTBEAT FROM SOCKET", NULL);
		}
		
		val_send=0, val_rec=0;		
		clock_gettime(CLOCK_MONOTONIC, &tm);
		tm.tv_sec += 3;
	        tm.tv_nsec = 0;	
		if((mq_timedsend(hblog_q, &val_send, sizeof(val_send),0, &tm))==-1)
        	{
			if((hblog_count++) > 10)
				LOG(ERROR, MAIN_TASK, "No heartbeat for LOG", NULL);
        	}
		
		clock_gettime(CLOCK_MONOTONIC, &tm);
		tm.tv_sec += 3;
		tm.tv_nsec = 0; 
		if((mq_receive(hblog_q, &val_rec, sizeof(val_rec), 0))==-1)
		{
			if((hblog_count++) > 10)
				LOG(ERROR, MAIN_TASK, "No heartbeat for LOG", NULL);
		}
		else if(val_rec == (val_send+1))
		{
			LOG(HEARTBEAT, LOG_TASK, "ALIVE: HEARTBEAT FROM LOG TASK", NULL);
		}
		val_rec=0;
		val_send=0;
		sleep(1);
	}
	//cleanup_process();
	//cleanup_queue();
	exit(1);	
}

void LOG(uint8_t loglevel, uint8_t log_source, char *msg, float *value)
{
	Logger_t logging;
        logging.timestamp=time(NULL);
        logging.log_level = loglevel;
        logging.log_ID = log_source;
	if(value!=NULL)
	logging.value = *value;
        memcpy(logging.message, msg, MSG_SIZE);
        if((mq_send(logger_q, &logging, sizeof(Logger_t),0))==-1)
        {
                printf("cant send message to process1 and returned %d\n", errno);
        }
}

