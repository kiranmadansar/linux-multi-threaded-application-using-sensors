#include <stdio.h>
#include <stdint.h>
#include "log.h"
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <mqueue.h>
#include <errno.h>
#include <signal.h>
#include "project.h"

volatile sig_atomic_t kill_process=0;
volatile sig_atomic_t kill_thread=0;
FILE *fp_log;
Logger_t log;
uint8_t thread_status;
pthread_t heartbeat_thread;
mqd_t hblog_q;
sem_t *log_lock, *my_sem;

void signal_handler()
{
	kill_thread =1;
	kill_process =1;
	if(!fp_log)
	{
		fflush(fp_log);
        	fclose(fp_log);
		fp_log = NULL;
	}
	sem_close(my_sem);
	mq_close(logger_q);
	mq_unlink(LOG_QUEUE);
	mq_close(hblog_q);
	mq_unlink(HBLOG_QUEUE);
	pthread_cancel(heartbeat_thread);
	pthread_join(heartbeat_thread, NULL);
	exit(1);
}


void *heartbeat_func(void *un)
{
	uint8_t val;
	while(!kill_thread)
	{
		if((mq_receive(hblog_q, &val, sizeof(val), 0))==ETIMEDOUT)
        	{
        		LOG(ERROR, MAIN_TASK, "No heartbeat for TEMP", NULL);
        	}
		val = val+1;
		if((mq_send(hblog_q, &val, sizeof(val),0))==-1)
                {
                        printf("cant send message to process1 and returned %d\n", errno);
                }
	}
}

int main(int argc, char *argv[])
{
	if((my_sem = sem_open(SEM_NAME, O_CREAT | O_RDWR, 0666, 1))==SEM_FAILED)
	{
		perror("sem failed:");
		return -1;
	}
	sem_wait(my_sem);
	uint8_t result;
	signal(SIGINT, signal_handler);
	char *filename = argv[1];
	fp_log = fopen(filename, "wb");
        
	struct mq_attr attr;
	attr.mq_maxmsg = 5;
        attr.mq_msgsize = sizeof(int);
	if((hblog_q = mq_open(HBLOG_QUEUE, O_RDWR, 0666, &attr))==-1)
        {
                perror("HB_TEMP QUEUE");
                exit(1);
        }

	result = pthread_create(&heartbeat_thread, NULL, heartbeat_func, NULL);
        if(result)
        {
                perror("HEART_BEAT THREAD");
                sem_close(log_lock);
		mq_close(hblog_q);
                exit(1);
        }

        if(!fp_log)
	{
		printf("File can't be opened\n");
                exit(1);
	}        
	fprintf(fp_log, "Timestamp\t");
        fprintf(fp_log, "LOG_LEVEL\t");
        fprintf(fp_log, "LOG_ID\t");
        fprintf(fp_log, "Message\n");
        fflush(fp_log);
        fclose(fp_log);
        fp_log = NULL;

	struct mq_attr attr_log;
	attr_log.mq_maxmsg = 5;
	attr_log.mq_msgsize = sizeof(Logger_t);

	if((logger_q = mq_open(LOG_QUEUE, O_RDWR, 0666, &attr_log))==-1)
	{
		perror("LOGGER QUEUE");
		exit(1);
	}

	LOG(INIT, LOG_TASK, "LOGGER TASK INITIALISED", NULL);

	while(!kill_process)
	{
	
		fp_log = fopen(filename, "a");
        	if(!fp_log)
                	exit(1);
		if((mq_receive(logger_q, &log, sizeof(Logger_t), 0))==-1)
		{
			printf("Din't receive message from process 2 and returned %d\n", errno);
		}
        	fprintf(fp_log, "%lu\t",log.timestamp);
        	fprintf(fp_log, "%u\t\t", log.log_level);
        	fprintf(fp_log, "%u\t", log.log_ID);
		if(((log.log_ID == TEMP_TASK) || (log.log_ID == LIGHT_TASK)) && (log.log_level == INFO))
		{
			fprintf(fp_log, "%s\t", log.message);
			fprintf(fp_log, "%f\n", (log.value));
		}
		else
		{
			fprintf(fp_log, "%s\n", log.message);
		}
		fflush(fp_log);
		fclose(fp_log);
		fp_log = NULL;
		usleep(50);
	}
	if(!fp_log)
	{
		fflush(fp_log);
        	fclose(fp_log);
		fp_log = NULL;
	}
	mq_close(logger_q);
	mq_unlink(LOG_QUEUE);
	mq_close(hblog_q);
	mq_unlink(HBLOG_QUEUE);
	pthread_join(heartbeat_thread, NULL);
	exit(1);
}

void LOG(uint8_t loglevel, uint8_t log_source, char *msg, float *value)
{
	Logger_t logging;
        logging.timestamp=time(NULL);
        logging.log_level = loglevel;
        logging.log_ID = log_source;
	if(value!=NULL)
	logging.value = *value;
        memcpy(logging.message, msg, MSG_SIZE);
        if((mq_send(logger_q, &logging, sizeof(Logger_t),0))==-1)
        {
                printf("cant send message to process1 and returned %d\n", errno);
        }
}

